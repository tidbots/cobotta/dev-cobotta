# dev-cobotta

[Cobotta](https://www.denso-wave.com/ja/robot/product/collabo/cobotta.html)を動かす


# 
[DockerでROSを動かせるようにする(Melodic)](https://zenn.dev/kenfuku/scraps/38827c0d8b89c2)


# under construction

# デンソー公式
[denso_cobotta_ros’s documentation](https://densorobot.github.io/docs/denso_cobotta_ros/index.html)

- Ubuntu-18.04 desktop
- ROS Melodic
- [COBOTTA driver for Linux](https://www.denso-wave.com/en/robot/download/application/cobotta_driver_for_linux.html)

# ホストPCの準備
## Dockerのインストール
お使いのホストPCにDockerをインストールします。

## Cobotta開発用Dockerイメージの作成
### リポジトリのダウンロード
本リポジトリをダウンロードします。
```
(host)$ cd ~
(host)$ git clone https://gitlab.com/tidbots/cobotta/dev-cobotta.git
```
### .envファイルの修正
docker compose の設定ファイルである、 .env ファイルをご自分の環境に合わせて修正して下さい。
```
(host)$ cd ~/dev-kachaka/docker
(host)$ cat .env
```
```
USER_NAME=[ユーザ名]
GROUP_NAME=[グループ名]
UID=[ユーザID]
GID=[グループID]
PASSWORD=[パスワード]
WORKSPACE_DIR=./ros_ws
```

ここで、WORKSPACE_DIRはDockerコンテナとホストPCで共有するディレクトリです。
自信が開発するワークスペースなどを指定してください。

### Dockerイメージの作成
下記のコマンドでDockerイメージを作成します。
```
(host)$ cd ~/dev-cobotta
(host)$ docker compose build
```

エラーが出なければ kachaka開発用のDockerイメージの作成は成功です。

### Dockerイメージの実行
```
(host)$ cd ~/dev-cobotta
(host)$ docker compose up
```

### Cobotta シミュレータの実行
```
(docker)$ roslaunch denso_cobotta_bringup denso_cobotta_bringup.launch sim:=true gripper_type:=none
```

お楽しみを！

# リンク集
